<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Post;


use Illuminate\Http\Request;
use Symfony\Component\Routing\Loader\ProtectedPhpFileLoader;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$postingan = DB::table('postingan')->get;
        $postingan = Post::latest()->get();

        return view('home.index', compact('postingan'));
    }

    public function post()
    {
        return view('post.show');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jenis = $request->get('jenis');
        $id = Auth::user()->id;
        $post = new Post;

        if ($jenis == 1) {
            $request->validate([
                'judul' => 'required|unique:postingan',
                'tulisan' => 'required'
            ]);
            $post->judul = $request["judul"];
            $post->tulisan = $request["tulisan"];
            $post->jenis = $jenis;
            $post->user_id = $id;
            $post->save();
            return redirect('/')->with('success', 'Post berhasil disimpan!');
        } else if ($jenis == 2) {
            $request->validate([
                'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
                'caption' => 'required'
            ]);

            if ($files = $request->file('gambar')) {
                $destinationPath = public_path('/images/');
                $postImage = "post" . date('YmdHis') . "." . $files->getClientOriginalExtension();
                $files->move($destinationPath, $postImage);

                $insert['image'] = "$postImage";
                $post->gambar = "$postImage";
                $post->caption = $request["caption"];
                $post->user_id = $id;
                $post->jenis = $jenis;
                $post->save();
            }
            return redirect('/')->with('success', 'Post berhasil disimpan!');
        } else if ($jenis == 3) {
            $request->validate([
                'quote' => 'required'
            ]);
            $post->quote = $request["quote"];
            $post->user_id = $id;
            $post->jenis = $jenis;
            $post->save();
            return redirect('/')->with('success', 'Post berhasil disimpan!');
        }


        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $table = "postingan";
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

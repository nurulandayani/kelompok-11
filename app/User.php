<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = ['username', 'name', 'email', 'password'];
    public function profil()
    {
        return $this->belongsTo('App\Profil');
    }
}

@extends('layout.master')

@section('content')

<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav ml-auto">
        <!-- Authentication Links -->
        @guest
        <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
        @if (Route::has('register'))
        <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
        </li>
        @endif
        @else
        <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
        @endguest
    </ul>
</div>
<div class="row ">
    <div class="col-md-12 gedf-main">
        <!-- Banner -->
        <div class="card gedf-card">
            <img class="card-img-top rounded-bottom" src="sosmed/images/share.jpg" alt="Card image cap">
        </div>
    </div>
</div>
<div class="row mt-2">
    <div class="col-md-8 gedf-main">
        @include('post.create')
        <!-- Post -->
        @foreach ($postingan as $key)
        @if($key->jenis==1)
        @include('layout.post')
        @elseif($key->jenis==2)
        @include('layout.post_image')
        @else
        @include('layout.post_quote')
        @endif
        @endforeach

    </div>
    <!-- Pertemanan -->
    <div class="col-md-4">
        @include ('layout.friends')
    </div>
</div>
@endsection
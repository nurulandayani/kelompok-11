<!DOCTYPE html>
<html lang="en">

<head>
    <title>Blogue</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @include('script.link')
</head>


<body>
    <div class="hero-wrap js-fullheight" style="background-image: url(sosmed/images/blog2.jpg);">


        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-6">
                <div class="container mt-5">

                    <a href="index.html"><img src="sosmed/images/blogue-white.png" width="300"></a>

                    <div class="row no-gutters slider-text" data-scrollax-parent="true">
                        <div class="col-md-6 ftco-animate">
                            <span class="heading">Welcome </span>
                            <h1 class="mb-4 text-white">We Are Online Platform to Share your Thoughts</h1>
                            <p class="caps">“The more you share, the more you receive to share.”</p>
                            <p class="mb-0"><a href="#" class="btn btn-primary">Start to Post</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 align-content-left">
                <!-- Login form -->
                <div id="logreg-forms" class="rounded">
                    <form class="form-signin">
                        <h1 class="h3 mb-3 font-weight-normal" style="text-align: center"> Sign in</h1>
                        <div class="social-login">
                            <button class="btn facebook-btn social-btn" type="button"><span><i class="fab fa-facebook-f"></i> Sign in with Facebook</span> </button>
                            <button class="btn google-btn social-btn" type="button"><span><i class="fab fa-google-plus-g"></i> Sign in with Google+</span> </button>
                        </div>
                        <p style="text-align:center"> OR </p>
                        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">
                        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="">

                        <button class="btn btn-success btn-block" type="submit"><i class="fas fa-sign-in-alt"></i> Sign in</button>
                        <a href="#" id="forgot_pswd">Forgot password?</a>
                        <hr>
                        <!-- <p>Don't have an account!</p>  -->
                        <button class="btn btn-primary btn-block" type="button" id="btn-signup"><i class="fas fa-user-plus"></i> Sign up New Account</button>
                    </form>

                    <form action="/reset/password/" class="form-reset">
                        <input type="email" id="resetEmail" class="form-control" placeholder="Email address" required="" autofocus="">
                        <button class="btn btn-primary btn-block" type="submit">Reset Password</button>
                        <a href="#" id="cancel_reset"><i class="fas fa-angle-left"></i> Back</a>
                    </form>

                    <form action="/signup/" class="form-signup">
                        <div class="social-login">
                            <button class="btn facebook-btn social-btn" type="button"><span><i class="fab fa-facebook-f"></i> Sign up with Facebook</span> </button>
                        </div>
                        <div class="social-login">
                            <button class="btn google-btn social-btn" type="button"><span><i class="fab fa-google-plus-g"></i> Sign up with Google+</span> </button>
                        </div>

                        <p style="text-align:center">OR</p>

                        <input type="text" id="user-name" class="form-control" placeholder="Full name" required="" autofocus="">
                        <input type="email" id="user-email" class="form-control" placeholder="Email address" required autofocus="">
                        <input type="password" id="user-pass" class="form-control" placeholder="Password" required autofocus="">
                        <input type="password" id="user-repeatpass" class="form-control" placeholder="Repeat Password" required autofocus="">

                        <button class="btn btn-primary btn-block" type="submit"><i class="fas fa-user-plus"></i> Sign Up</button>
                        <a href="#" id="cancel_signup"><i class="fas fa-angle-left"></i> Back</a>
                    </form>
                    <br>

                </div>
            </div>
        </div>
    </div>




    @include('script.script')

</body>

</html>
<div class="card gedf-card">
    <div class="card-header">
        <h5><strong>Friends</strong></h5>
    </div>
    <div class="card-body">
        <h6 class="card-subtitle mb-4 text-muted">Most active friends</h6>
        <div class="d-flex justify-content-between align-items-center friend-state mt-2">
            <div class="d-flex">
                <div>
                    <img class="rounded-circle" src="https://picsum.photos/80/80/?random?image=1" width="40" alt="">
                </div>
                <div class="ml-2 h7">
                    Alisa Anesti
                </div>
            </div>
            <div class="state"></div>
        </div>
        <div class="d-flex justify-content-between align-items-center friend-state mt-2">
            <div class="d-flex">
                <div>
                    <img class="rounded-circle" src="https://picsum.photos/80/80/?random?image=2" width="40" alt="">
                </div>
                <div class="ml-2 h7">
                    Amara Putri
                </div>
            </div>
            <div class="state"></div>
        </div>
        <div class="d-flex justify-content-between align-items-center friend-state mt-2">
            <div class="d-flex">
                <div>
                    <img class="rounded-circle" src="https://picsum.photos/80/80/?random?image=3" width="40" alt="">
                </div>
                <div class="ml-2 h7">
                    Intan Nuraini
                </div>
            </div>
            <div class="state"></div>
        </div>
        <div class="d-flex justify-content-between align-items-center friend-state mt-2">
            <div class="d-flex">
                <div>
                    <img class="rounded-circle" src="https://picsum.photos/80/80/?random?image=4" width="40" alt="">
                </div>
                <div class="ml-2 h7">
                    Rajendra
                </div>
            </div>
            <div class="state"></div>
        </div>
        <div class="d-flex justify-content-between align-items-center friend-state mt-2">
            <div class="d-flex">
                <div>
                    <img class="rounded-circle" src="https://picsum.photos/80/80/?random?image=5" width="40" alt="">
                </div>
                <div class="ml-2 h7">
                    Ahmad Khairi
                </div>
            </div>
            <div class="state"></div>
        </div>
        <div class="d-flex justify-content-between align-items-center friend-state mt-2">
            <div class="d-flex">
                <div>
                    <img class="rounded-circle" src="https://picsum.photos/80/80/?random?image=6" width="40" alt="">
                </div>
                <div class="ml-2 h7">
                    Fernando Hose
                </div>
            </div>
            <div class="state"></div>
        </div>
        <div class="d-flex justify-content-between align-items-center friend-state mt-2">
            <div class="d-flex">
                <div>
                    <img class="rounded-circle" src="https://picsum.photos/80/80/?random?image=7" width="40" alt="">
                </div>
                <div class="ml-2 h7">
                    Aldo Giovanni
                </div>
            </div>
            <div class="state"></div>
        </div>
        <div class="d-flex justify-content-between align-items-center friend-state mt-2">
            <div class="d-flex">
                <div>
                    <img class="rounded-circle" src="https://picsum.photos/80/80/?random?image=8" width="40" alt="">
                </div>
                <div class="ml-2 h7">
                    Imran Nababan
                </div>
            </div>
            <div class="state"></div>
        </div>
        <div class="d-flex justify-content-between align-items-center friend-state mt-2">
            <div class="d-flex">
                <div>
                    <img class="rounded-circle" src="https://picsum.photos/80/80/?random?image=9" width="40" alt="">
                </div>
                <div class="ml-2 h7">
                    Ken Zhi
                </div>
            </div>
            <div class="state"></div>
        </div>
        <div class="d-flex justify-content-between align-items-center friend-state mt-2">
            <div class="d-flex">
                <div>
                    <img class="rounded-circle" src="https://picsum.photos/80/80/?random?image=10" width="40" alt="">
                </div>
                <div class="ml-2 h7">
                    Dicky Iskandar
                </div>
            </div>
            <div class="state"></div>
        </div>
    </div>



</div>
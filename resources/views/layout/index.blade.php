@extends('layout.master')

@section('content')
<div class="row ">
    <div class="col-md-12 gedf-main">
        <!-- Banner -->
        <div class="card gedf-card">
            <img class="card-img-top rounded-bottom" src="sosmed/images/share.jpg" alt="Card image cap">
        </div>
    </div>
</div>
<div class="row mt-2">
    <div class="col-md-8 gedf-main">
        @include('post.create')
        <!-- Post -->
        @foreach ($postingan as $key)
        @if($key->jenis==1)
        @include('layout.post')
        @elseif($key->jenis==2)
        @include('layout.post_image')
        @else
        @include('layout.post_quote')
        @endif
        @endforeach

    </div>
    <!-- Pertemanan -->
    <div class="col-md-4">
        @include ('layout.friends')
    </div>
</div>


@endsection
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Herr+Von+Muellerhoff" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('sosmed/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('sosmed/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('sosmed/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('sosmed/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('sosmed/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('sosmed/css/aos.css')}}">
    <link rel="stylesheet" href="{{asset('sosmed/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('sosmed/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('sosmed/css/jquery.timepicker.css')}}">
    <link rel="stylesheet" href="{{asset('sosmed/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('sosmed/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('sosmed/css/style.css')}}">

</head>


<body>

    <div id="colorlib-page">
        <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
        <aside id="colorlib-aside" role="complementary" class="js-fullheight text-center">
            <div class="text-center">
                <img src="sosmed/images/logo_blogue.png" width="150">
            </div>

            <h1 id="colorlib-logo"><a href="/home"><span class="img" style="background-image: url(/images/fotoku.jpeg);"></span>{{ Auth::user()->name }}</a></h1>

            <nav id="colorlib-main-menu" role="navigation">
                <ul>
                    <li class="colorlib-active"><a href="/home">Home</a></li>
                    <li><a href="/post">My Post</a></li>
                    <li><a href="/profil">About Me</a></li>
                    <li><a href="/pertemanan">Friends</a></li>
                </ul>
            </nav>
            <div class="card">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="h6 text-muted">Followers</div>
                        <div class="h5">20</div>
                    </li>
                    <li class="list-group-item">
                        <div class="h6 text-muted">Following</div>
                        <div class="h5">10</div>
                    </li>
                </ul>
            </div>

        </aside> <!-- END COLORLIB-ASIDE -->

        <div id="colorlib-main">
            <div class="container-fluid gedf-wrapper mt-2 mb-2">
                <div class="row">
                    <div class="navbar-expand-md col-md-12 gedf-main">
                        @include('layouts.nav')
                    </div>
                </div>
                @yield('content')

            </div>

            <footer class="ftco-footer ftco-bg-dark">
                <div class="text-center">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                        </script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </footer>
        </div><!-- END COLORLIB-MAIN -->
    </div><!-- END COLORLIB-PAGE -->


    <!-- loader -->



    @include('script.script')

</body>

</html>
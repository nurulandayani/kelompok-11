 <!--- \\\\\\\Post-->
 <div class="card gedf-card mt-2">
     <div class="card-header">
         <div class="d-flex justify-content-between align-items-center">
             <div class="d-flex justify-content-between align-items-center">
                 <div class="mr-2">
                     <img class="rounded-circle" width="45" height="45" src="https://picsum.photos/80/80/?random?image=17" alt="">
                 </div>
                 <div class="ml-2">
                     <div class="h5 m-0">{{$key->User->name}}</div>
                     <div class="h7 text-muted">Me and myself</div>
                 </div>
             </div>

         </div>

     </div>
     <div class="card-body">
         <div class="text-muted h7 mb-2">posted in <i class="fa fa-clock-o"></i>{{$key->created_at}}</div>
         <a class="card-link" href="#" data-toggle="modal">
             <h5 class=" card-title">{{$key -> judul}}</h5>
         </a>

         <p class=" card-text">
             {{$key->tulisan}}
         </p>

     </div>
     <div class="card-footer">
         <a href="#" class="card-link"><i class="fa fa-gittip"></i> Like</a>
         <a href="#" class="card-link"><i class="fa fa-comment"></i> Comment</a>
         <a href="#" class="card-link"><i class="fa fa-mail-forward"></i> Share</a>
     </div>
 </div>
 <!-- Post /////-->
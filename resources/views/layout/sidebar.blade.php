<a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
<aside id="colorlib-aside" role="complementary" class="js-fullheight text-center">
    <div class="text-center">
        <img src="sosmed/images/logo_blogue.png" width="150">
    </div>

    <h1 id="colorlib-logo"><a href="index.html"><span class="img" style="background-image: url(sosmed/images/author.jpg);"></span>{{ Auth::user()->name }}</a></h1>

    <nav id="colorlib-main-menu" role="navigation">
        <ul>
            <li class="colorlib-active"><a href="/home">Home</a></li>
            <li><a href="/post">My Post</a></li>
            <li><a href="/profil">About Me</a></li>
            <li><a href="/pertemanan">Friends</a></li>
        </ul>
    </nav>
    <div class="card">
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <div class="h6 text-muted">Followers</div>
                <div class="h5">20</div>
            </li>
            <li class="list-group-item">
                <div class="h6 text-muted">Following</div>
                <div class="h5">10</div>
            </li>
        </ul>
    </div>

</aside> <!-- END COLORLIB-ASIDE -->
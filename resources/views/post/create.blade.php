<!--- Post -->
<div class="card gedf-card ">
    <div class="card-header">
        <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="posts-tab" data-toggle="tab" href="#posts" role="tab" aria-controls="posts" aria-selected="true">Make
                    a post</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="images-tab" data-toggle="tab" role="tab" aria-controls="images" aria-selected="false" href="#images">Images</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="quote-tab" data-toggle="tab" role="tab" aria-controls="quote" aria-selected="false" href="#quote">Quotes</a>
            </li>
        </ul>
    </div>
    <div class="card-body">


        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                <form action="/post" method="POST">
                    @csrf
                    <div class="form-group">
                        <input type="hidden" name="jenis" id="jenis" value="1" />
                        <input type="text" class="form-control" id="judul" placeholder="Title" name="judul">
                        @error('judul')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror

                        <textarea class="form-control mt-2" id="tulisan" name="tulisan" rows="3" placeholder="What are you thinking?"></textarea>
                        @error('tulisan')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                        <div class="btn-group mt-2">
                            <button type="submit" class="btn btn-primary">post</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
                <form action="/post" method="POST" enctype="multipart/form-data" accept-charset="utf-8">
                    @csrf

                    <div class="form-group">
                        <input type="hidden" name="jenis" id="jenis" value="2" />
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="gambar" onchange="readURL(this);">
                            <label class="custom-file-label" for="customFile"></label>
                            @error('gambar')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="custom-file mt-2 mb-2">
                            <input type="text" class="form-control" id="caption" placeholder="caption" name="caption">
                            @error('caption')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <br>
                        <div class="btn-group mt-2">
                            <button type="submit" class="btn btn-primary" value="post">post</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="tab-pane fade " id="quote" role="tabpanel" aria-labelledby="quote-tab">
                <form action="/post" method="POST">
                    @csrf
                    <div class="form-group">
                        <input type="hidden" name="jenis" id="jenis" value="3" />
                        <input type="text" class="form-control" id="quote" placeholder="Post a quote" name="quote">
                        @error('quote')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="btn-group mt-2">
                        <button type="submit" class="btn btn-primary">post</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
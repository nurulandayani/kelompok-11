<!DOCTYPE html>
<html lang="en">

<head>
    <title>Blogue</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @include('script.link')
</head>


<body>

    <div id="colorlib-page">
        <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
        <aside id="colorlib-aside" role="complementary" class="js-fullheight text-center">
            <div class="mr-2 mb-2 text-center">
                <img src="sosmed/images/logo_blogue.png" width="150">
            </div>

            <h1 id="colorlib-logo"><a href="index.html"><span class="img" style="background-image: url(sosmed/images/author.jpg);"></span>Louie Smith</a></h1>
            <nav id="colorlib-main-menu" role="navigation">
                <ul>
                    <li class="colorlib-active"><a href="/home">Home</a></li>
                    <li><a href="/post">My Post</a></li>
                    <li><a href="/profil">About Me</a></li>
                    <li><a href="/friends">Friends</a></li>
                </ul>
            </nav>
            <div class="card">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="h6 text-muted">Followers</div>
                        <div class="h5">20</div>
                    </li>
                    <li class="list-group-item">
                        <div class="h6 text-muted">Following</div>
                        <div class="h5">10</div>
                    </li>
                </ul>
            </div>

        </aside> <!-- END COLORLIB-ASIDE -->

        <div id="colorlib-main">
            <div class="container-fluid gedf-wrapper mt-2">
                <div class="row ">
                    <div class="col-md-12 gedf-main">
                        <!--- Banner -->
                        <div class="card gedf-card ">
                            <img class="card-img-top rounded" src="sosmed/images/gunung.jpg" alt="Card image cap">
                        </div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-8 gedf-main">

                        <!--- Post -->
                        <div class="card gedf-card ">
                            <div class="card-header">
                                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="posts-tab" data-toggle="tab" href="#posts" role="tab" aria-controls="posts" aria-selected="true">Make
                                            a post</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="images-tab" data-toggle="tab" role="tab" aria-controls="images" aria-selected="false" href="#images">Images</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="images-tab" data-toggle="tab" role="tab" aria-controls="images" aria-selected="false" href="#tagline">Tagline</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                                        <div class="form-group">
                                            <label class="sr-only" for="message">post</label>
                                            <textarea class="form-control" id="message" rows="3" placeholder="What are you thinking?"></textarea>
                                        </div>

                                    </div>
                                    <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
                                        <div class="form-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Upload image</label>
                                            </div>
                                            <div class="custom-file mt-2">
                                                <input type="text" class="form-control" id="caption" placeholder="caption">
                                            </div>
                                        </div>
                                        <div class="py-4"></div>
                                    </div>
                                    <div class="tab-pane fade show active" id="quote" role="tabpanel" aria-labelledby="posts-tab">
                                        <div class="form-group">
                                            <label class="sr-only" for="quote">Quote</label>
                                            <input type="text" class="form-control" id="quote" placeholder="Post Quote">
                                        </div>

                                    </div>
                                </div>
                                <div class="btn-toolbar justify-content-between">
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-primary">post</button>
                                    </div>
                                    <div class="btn-group">
                                        <button id="btnGroupDrop1" type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-globe"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                                            <a class="dropdown-item" href="#"><i class="fa fa-globe"></i> Public</a>
                                            <a class="dropdown-item" href="#"><i class="fa fa-users"></i> Friends</a>
                                            <a class="dropdown-item" href="#"><i class="fa fa-user"></i> Just me</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Post -->

                        @include('layout.post_image')
                        @include ('layout.post')
                        @include ('layout.post')

                    </div>
                    <!-- Pertemanan-->
                    <div class="col-md-4">
                        @include ('layout.friends')
                    </div>
                </div>
            </div>

            <footer class="ftco-footer ftco-bg-dark ftco-section mt-2">
                <div class="container px-md-3">

                    <div class="row">
                        <div class="col-md-12 text-center">

                            <p>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>
                                    document.write(new Date().getFullYear());
                                </script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div><!-- END COLORLIB-MAIN -->
    </div><!-- END COLORLIB-PAGE -->

    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen mt-2"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" />
        </svg></div>


    @include('script.script')

</body>

</html>
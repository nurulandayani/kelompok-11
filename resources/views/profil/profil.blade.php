<!DOCTYPE html>
<html lang="en">

<head>
    <title>Blogue</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @include('script.link')
</head>


<body>

    <div id="colorlib-page">
        <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
        <aside id="colorlib-aside" role="complementary" class="js-fullheight text-center">
            <div class="text-center">
                <img src="sosmed/images/logo_blogue.png" width="150">
            </div>

            <h1 id="colorlib-logo"><a href="index.html"><span class="img" style="background-image: url(sosmed/images/author.jpg);"></span>Louie Smith</a></h1>
            <nav id="colorlib-main-menu" role="navigation">
                <ul>
                    <li class="colorlib-active"><a href="/home">Home</a></li>
                    <li><a href="/post">My Post</a></li>
                    <li><a href="/profil">About Me</a></li>
                    <li><a href="/pertemanan">Friends</a></li>
                </ul>
            </nav>
            <div class="card">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="h6 text-muted">Followers</div>
                        <div class="h5">20</div>
                    </li>
                    <li class="list-group-item">
                        <div class="h6 text-muted">Following</div>
                        <div class="h5">10</div>
                    </li>
                </ul>
            </div>

        </aside> <!-- END COLORLIB-ASIDE -->

        <div id="colorlib-main">
            <section class="ftco-section-no-padding bg-light">
                <div class="hero-wrap">
                    <div class="overlay"></div>
                    <div class="d-flex align-items-center js-fullheight">
                        <div class="author-image text img d-flex">
                            <section class="home-slider js-fullheight owl-carousel">
                                <div class="slider-item js-fullheight" style="background-image: url(sosmed/images/author.jpg);">
                                </div>

                                <div class="slider-item js-fullheight" style="background-image:url(sosmed/images/author-2.jpg);">
                                </div>
                            </section>
                        </div>
                        <div class="author-info text p-3 p-md-5">
                            <div class="desc">
                                <span class="subheading">Hello! I'm</span>
                                <h1 class="big-letter">Louie Smith</h1>
                                <h1 class="mb-4"><span>Louie Smith</span> A Photographer. <span>I Capture Life</span></h1>
                                <p class="mb-4">I am A Photographer from America Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                                <h3 class="signature h1">Louie Smith</h3>
                                <ul class="ftco-social mt-3">
                                    <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                                    <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                                    <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <footer class="ftco-footer ftco-bg-dark ftco-section">
                <div class="container px-md-5">
                    <div class="row">
                        <div class="col-md-12">

                            <p>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>
                                    document.write(new Date().getFullYear());
                                </script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div><!-- END COLORLIB-MAIN -->
    </div><!-- END COLORLIB-PAGE -->



    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen mt-2"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" />
        </svg></div>


    @include('script.script')

</body>

</html>
<link href="{{asset('sosmed/css/bootstrap.min.css')}}" rel="stylesheet" id="bootstrap-css">

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Herr+Von+Muellerhoff" rel="stylesheet">
<link rel="stylesheet" href="{{asset('sosmed/css/open-iconic-bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('sosmed/css/animate.css')}}">
<link rel="stylesheet" href="{{asset('sosmed/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('sosmed/css/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="{{asset('sosmed/css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{asset('sosmed/css/aos.css')}}">
<link rel="stylesheet" href="{{asset('sosmed/css/ionicons.min.css')}}">
<link rel="stylesheet" href="{{asset('sosmed/css/bootstrap-datepicker.css')}}">
<link rel="stylesheet" href="{{asset('sosmed/css/jquery.timepicker.css')}}">
<link rel="stylesheet" href="{{asset('sosmed/css/flaticon.css')}}">
<link rel="stylesheet" href="{{asset('sosmed/css/icomoon.css')}}">
<link rel="stylesheet" href="{{asset('sosmed/css/style.css')}}">
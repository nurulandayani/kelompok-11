<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Auth
Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

// CRUD Post

Route::get('/post', 'BlogController@post');
Route::get('/post/create', 'BlogController@create');
Route::post('/post', 'BlogController@store');



Route::get('/profil', 'ProfilController@index');


// CRUD Pertemanan

Route::resource('pertemanan', 'PertemananController');
Route::get('/pertemanan', 'PertemananController@index')->middleware('auth');
Route::get('/pertemanan/create', 'PertemananController@create');
Route::post('pertemanan', 'PertemananController@store');
Route::get('/pertemanan/{pertemanan_id}', 'PertemananController@show');
Route::get('/pertemanan/{pertemanan_id}', 'PertemananController@edit');
Route::put('/pertemanan/{pertemanan_id}', 'PertemananController@update');
Route::delete('/pertemanan/{pertemanan_id}', 'PertemananController@destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
